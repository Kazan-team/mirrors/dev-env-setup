#!/bin/bash
if [ "$EUID" -ne 0 ]
  then echo "Please run as root using 'sudo bash'"
  exit
fi

# this is a bit of a cheat, by relying on debian to pull in all the
# build dependencies.  saves time researching how to get and list them
# by hand.
apt-get update -y
apt-get build-dep yosys -y
apt-get remove yosys -y
apt-get build-dep ghdl -y # this is sort-of what ghdl-install does, hmmm
apt-get install -y default-jre-headless python3-pip curl
python3 -m pip install toml

mkdir -p /home/$SUDO_USER/src
cd /home/$SUDO_USER/src
mkdir -p hdl_tools
cd hdl_tools

git clone https://git.libre-soc.org/git/yosys.git
git clone https://git.libre-soc.org/git/SymbiYosys.git sby
git clone https://github.com/SRI-CSL/yices2.git
git clone https://github.com/Z3Prover/z3.git
# XXX see ghdl-install this is really best done with that
git clone https://github.com/ghdl/ghdl
git clone https://github.com/ghdl/ghdl-yosys-plugin
# both of these need adding https://bugs.libre-soc.org/show_bug.cgi?id=883
git clone https://github.com/bitwuzla/bitwuzla.git
git clone -b cvc5-1.0.1 https://github.com/cvc5/cvc5.git cvc5

# yosys 0.13 has been found to be stable...
cd yosys
git checkout smtlib2-expr-support-on-0.13
make config-clang
make -j$(nproc)
make install

# ... but things are in the middle of a transition so use this version of ghdl
cd ../ghdl
git checkout 263c843ed49f59cb4cc3038bafcac2b9238ebad4
./configure --with-llvm-config
make
make install

# ...with this specific version of the yosys-ghdl-plugin
cd ../ghdl-yosys-plugin
git checkout c9b05e481423c55ffcbb856fd5296701f670808c
make
make install

# ... cvc5
cd ../cvc5
git rev-parse HEAD
./configure.sh --poly --auto-download
cd build
make -j$(nproc)
make install

# ... bitwuzla
cd ../../bitwuzla
git checkout 19dd987a6e246990619751cca07996fac505fd0b
./contrib/setup-cadical.sh
./contrib/setup-btor2tools.sh
./contrib/setup-symfpu.sh
./configure.sh
cd build
make -j$(nproc)
make install

# symbiyosys should be fine though
cd ../../sby
make install

cd ../yices2
autoconf
./configure
make -j$(nproc)
make install

cd ../z3
python scripts/mk_make.py
cd build
make -j$(nproc)
make install

cd /home/$SUDO_USER/src
chown -R $SUDO_USER .
chgrp -R $SUDO_USER .
