#!/bin/bash
if [ "$EUID" -ne 0 ]
  then echo "Please run as root using 'sudo bash'"
  exit
fi

# change into $SUDO_USER home directory
cd /home/$SUDO_USER
mkdir -p src/fpga-boot-load-prog
cd src/fpga-boot-load-prog

apt-get install -y libcurl3-gnutls git git-man

git clone https://git.code.sf.net/p/dfu-util/dfu-util
git clone https://github.com/trabucayre/openFPGALoader.git
git clone https://github.com/f32c/tools.git
git clone https://github.com/kost/fujprog.git
git clone https://github.com/xtrx-sdr/xc3sprog.git
git clone https://github.com/gregdavill/ecpprog.git

# Steps to compile dfu-util

## Necessary software to install

apt-get install -y build-essential make autoconf libusb-dev \
 libusb-1.0-0-dev pkg-config

## Build dfu-util

cd dfu-util
git checkout v0.11
./autogen.sh
./configure
make -j$(nproc)
make install
cd ..

# Steps to compile openFPGALoader

## Necessary software to install

apt-get install -y cmake libftdi1-2 libftdi1-dev libhidapi-hidraw0 \
 libhidapi-dev libudev-dev zlib1g-dev

## Build openFPGALoader

cd openFPGALoader
git checkout v0.8.0
mkdir build; cd build
cmake ..
make -j$(nproc)
make install
cd ..
cp 99-openfpgaloader.rules /etc/udev/rules.d
cd ..

# Steps to compile ujprog

## Build ujprog

cd tools
git checkout 0698352b0e912caa9b8371b8f692e19aac547a69
cd ujprog
cp Makefile.linux Makefile
sed -i -e 's@ -static@@g' \
 -e 's@/usr/lib/${ARCHNAME}/libftdi.a@$(shell pkg-config --cflags --libs libftdi1)@g' \
 -e 's@^USBLIB@#USBLIB@' Makefile
sed -i -e 's@usb_reset@ftdi_usb_reset@g' ujprog.c
make
install -v -m 4755 ujprog /usr/local/bin

cat > /etc/udev/rules.d/80-fpga-ulx3s.rules << EOF
# this is for usb-serial tty device
SUBSYSTEM=="tty", ATTRS{idVendor}=="0403", ATTRS{idProduct}=="6015", \
 MODE="664", GROUP="dialout"
# this is for ujprog libusb access
ATTRS{idVendor}=="0403", ATTRS{idProduct}=="6015", \
 GROUP="dialout", MODE="666"
EOF
cd ../..

# Steps to compile fujprog

## Build fujprog

cd fujprog
git checkout v4.8
mkdir build; cd build
cmake ..
make -j$(nproc)
make install
cd ../..

# Steps to compile xc3sprog

## Build xc3sprog

cd xc3sprog
git checkout 99e7de20aa8323712e5f70ff74d2079d5fb45bc7
sed -i -e 's@^if(NOT LIBUSB3380_FOUND)@if(NOT LIBUSB3380_FOUND)\n  include(FindPkgConfig)@' Findlibusb3380.cmake
mkdir build; cd build
cmake -DLIBFTDI_FOUND=YES -DLIBFTDI_INCLUDE_DIR="/usr/include/libftdi1" \
 -DLIBFTDI_LIBRARIES="ftdi1" -DLIBUSB_FOUND=YES \
 -DLIBUSB_INCLUDE_DIR="/usr/include" -DLIBUSB_LIBRARIES="usb" \
 -DUSE_FTD2XX=OFF -DUSE_LIBUSB3380=OFF ..
make -j$(nproc)
make install
cd ../..

# Steps to compile ecpprog

## Build ecpprog

cd ecpprog
git checkout 7212b56a9d2fc6de534e06636a1c6d8b0c6f80ab
cd ecpprog
make
make install
cd ../..

# Install udev rules for boards

cat > /etc/udev/rules.d/90-ecp5.rules << EOF
SUBSYSTEM=="usb", ATTRS{idVendor}=="0403", ATTRS{idProduct}=="6020", \
 GROUP="users", MODE="0666"
EOF

cat > /etc/udev/rules.d/90-ftdi-orangecrab.rules << EOF
SUBSYSTEM=="usb", ATTRS{idVendor}=="1209", ATTRS{idProduct}=="5af0", \
 GROUP="users", MODE="0666"
EOF

cat > /etc/udev/rules.d/90-arty7-100t.rules << EOF
SUBSYSTEM=="usb", ATTRS{idVendor}=="0403", ATTRS{idProduct}=="6010", \
 GROUP="users", MODE="0666"
EOF

# Run finally this to update udev
# udevadm control --reload-rules && udevadm trigger
# usermod -a $USER -G plugdev

cd /home/$SUDO_USER/src/fpga-boot-load-prog
chown -R $SUDO_USER .
chgrp -R $SUDO_USER .

